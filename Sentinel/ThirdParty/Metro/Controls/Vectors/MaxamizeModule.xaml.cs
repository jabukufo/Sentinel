﻿using System.Windows.Controls;

namespace MetroWPFTemplate.Metro.Controls.Vectors
{
	/// <summary>
	/// Interaction logic for MaxamizeModule.xaml
	/// </summary>
	public partial class MaxamizeModule : UserControl
	{
		public MaxamizeModule()
		{
			this.InitializeComponent();
		}
	}
}