﻿using System.Windows.Controls;

namespace MetroWPFTemplate.Metro.Controls.Vectors
{
	/// <summary>
	/// Interaction logic for CloseModule.xaml
	/// </summary>
	public partial class CloseModule : UserControl
	{
		public CloseModule()
		{
			this.InitializeComponent();
		}
	}
}