﻿using System.Windows.Controls;

namespace MetroWPFTemplate.Metro.Controls.Vectors
{
    /// <summary>
    /// Interaction logic for ScreenshotModule.xaml
    /// </summary>
    public partial class ScreenshotModule : UserControl
    {
        public ScreenshotModule()
        {
            InitializeComponent();
        }
    }
}
