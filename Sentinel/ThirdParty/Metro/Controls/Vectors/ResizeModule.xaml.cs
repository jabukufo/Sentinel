﻿using System.Windows.Controls;

namespace MetroWPFTemplate.Metro.Controls.Vectors
{
    /// <summary>
    /// Interaction logic for ResizeModule.xaml
    /// </summary>
    public partial class ResizeModule : UserControl
    {
        public ResizeModule()
        {
            InitializeComponent();
        }
    }
}
