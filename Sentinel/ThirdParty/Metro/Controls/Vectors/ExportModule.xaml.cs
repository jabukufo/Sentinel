﻿using System.Windows.Controls;

namespace MetroWPFTemplate.Metro.Controls.Vectors
{
    /// <summary>
    /// Interaction logic for ExportModule.xaml
    /// </summary>
    public partial class ExportModule : UserControl
    {
        public ExportModule()
        {
            InitializeComponent();
        }
    }
}
