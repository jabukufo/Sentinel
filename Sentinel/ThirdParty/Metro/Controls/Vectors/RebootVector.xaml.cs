﻿using System.Windows.Controls;

namespace MetroWPFTemplate.Metro.Controls.Vectors
{
    /// <summary>
    /// Interaction logic for RebootVector.xaml
    /// </summary>
    public partial class RebootVector : UserControl
    {
        public RebootVector()
        {
            InitializeComponent();
        }
    }
}
