﻿using System.Windows.Controls;

namespace MetroWPFTemplate.Metro.Controls.Vectors
{
	/// <summary>
	/// Interaction logic for MinimizeModule.xaml
	/// </summary>
	public partial class MinimizeModule : UserControl
	{
		public MinimizeModule()
		{
			this.InitializeComponent();
		}
	}
}