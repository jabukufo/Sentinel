﻿using System.Windows.Controls;

namespace MetroWPFTemplate.Metro.Controls.Vectors
{
	/// <summary>
	/// Interaction logic for RestoreModule.xaml
	/// </summary>
	public partial class RestoreModule : UserControl
	{
		public RestoreModule()
		{
			this.InitializeComponent();
		}
	}
}