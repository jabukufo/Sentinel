﻿using System;
using System.Reflection;
using System.Windows.Controls;

namespace Sentinel.UI.Controls.Fields
{
    /// <summary>
    /// Interaction logic for EnumFieldControl.xaml
    /// </summary>
    public partial class EnumFieldControl : UserControl
    {
        FieldInfo field;

        public EnumFieldControl(FieldInfo field, object fieldValue)
        {
            InitializeComponent();

            this.field = field;

            for (int i = 0; i < Enum.GetValues(field.FieldType).Length; i++)
            {
                var cmbItem = new ComboBoxItem();
                cmbItem.Content = Enum.GetName(field.FieldType, i);

                cmbValue.Items.Add(cmbItem);
            }
            cmbValue.SelectedIndex = Array.IndexOf(Enum.GetValues(field.FieldType), fieldValue);

            lblName.Content = field.Name;
            lblType.Content = field.FieldType.Name;
        }
    }
}
