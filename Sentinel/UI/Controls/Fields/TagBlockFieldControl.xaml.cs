﻿using System.Collections;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using BlamCore.Cache;
using BlamCore.Serialization;

namespace Sentinel.UI.Controls.Fields
{
    /// <summary>
    /// Interaction logic for TagBlockFieldControl.xaml
    /// </summary>
    public partial class TagBlockFieldControl : UserControl
    {
        IList tagBlock;
        FieldInfo field;
        GameCacheContext CacheContext;

        public TagBlockFieldControl(FieldInfo field, IList tagBlock, GameCacheContext CacheContext)
        {
            InitializeComponent();

            this.tagBlock = tagBlock;
            this.field = field;
            this.CacheContext = CacheContext;

            lblName.Content = field.Name; //+ " | " + tagBlock.GetType().GetGenericArguments().FirstOrDefault();


            if (tagBlock == null || tagBlock.Count == 0)
            {
                cmbIndex.IsEnabled = false;
                blockStructure.Visibility = Visibility.Collapsed;
                btnExpand.IsEnabled = false;
                btnExpand.Content = "+";
            }
            else
            {
                for (int i = 0; i < tagBlock.Count; i++)
                    cmbIndex.Items.Add(i);

                cmbIndex.SelectedIndex = 0;
                SelectIndex(0);
            }
        }

        public void SelectIndex(int i)
        {
            if (i >= 0 && i < tagBlock.Count)
            {
                blockStructure.Children.Clear();

                if (tagBlock[i] != null && tagBlock[i].GetType().CustomAttributes.ToList().Find(a => a.AttributeType == typeof(TagStructureAttribute)) != null)
                    TagFieldControlGenerator.Generate(blockStructure, CacheContext, new TagStructureInfo(tagBlock[i].GetType(), CacheContext.Version), tagBlock[i]);
            }
        }

        private void cmbIndex_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectIndex(cmbIndex.SelectedIndex);
        }

        private void btnExpand_Click(object sender, RoutedEventArgs e)
        {
            blockStructure.Visibility = (blockStructure.Visibility == Visibility.Collapsed) ? Visibility.Visible : Visibility.Collapsed;
            btnExpand.Content = (blockStructure.Visibility == Visibility.Collapsed) ? "+" : "-";
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
