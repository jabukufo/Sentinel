﻿using System.Reflection;
using System.Windows.Controls;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.TagDefinitions;

namespace Sentinel.UI.Controls.Fields
{
    /// <summary>
    /// Interaction logic for TagReferenceFieldControl.xaml
    /// </summary>
    public partial class TagReferenceFieldControl : UserControl
    {
        GameCacheContext cacheContext;

        public TagReferenceFieldControl(FieldInfo field, CachedTagInstance fieldValue, GameCacheContext cacheContext)
        {
            InitializeComponent();

            this.cacheContext = cacheContext;

            // Add null option
            cmbType.Items.Add(new TagReferenceItem()
            {
                tag = null,
                value = "null"
            });
            cmbType.SelectedIndex = 0;

            foreach (var type in TagDefinition.Types)
            {
                cmbType.Items.Add(type.Key.ToString());
                // If this is the type of our tag then select it
                if (fieldValue != null && type.Value == TagDefinition.Find(fieldValue.Group.Tag))
                    cmbType.SelectedIndex = cmbType.Items.Count - 1;
            }

            // Populate tag list
            UpdateTagReferences();

            // Find tag reference index
            foreach(TagReferenceItem item in cmbReference.Items)
                if (item.tag.Index == fieldValue.Index)
                {
                    cmbReference.SelectedItem = item;
                    break;
                }

            lblName.Content = field.Name;
            lblType.Content = field.FieldType.Name;
        }

        public void UpdateTagReferences()
        {
            cmbReference.Items.Clear();
            if (cmbType.SelectedItem != null)
                foreach (var tag in cacheContext.TagCache.Index.FindAllInGroup(new Tag(cmbType.SelectedItem.ToString())))
                    cmbReference.Items.Add(new TagReferenceItem()
                    {
                        tag = tag,
                        value = $"[0x{tag.Index:X4}] {cacheContext.TagNames[tag.Index]}.{cacheContext.GetString(tag.Group.Name)}"
                    });
        }

        private void cmbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateTagReferences();

            if (cmbReference.Items.Count > 0)
                cmbReference.SelectedIndex = 0;
        }

        public class TagReferenceItem
        {
            public CachedTagInstance tag;
            public string value;

            public override string ToString()
            {
                return value;
            }
        }
    }
}
