﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.TagDefinitions;

namespace Sentinel.UI.Controls.Fields
{
    /// <summary>
    /// Interaction logic for StringIdFieldControl.xaml
    /// </summary>
    public partial class StringIdFieldControl : UserControl
    {
        GameCacheContext cacheContext;
        FieldInfo field;
        ObservableCollection<StringItem> recommendations = new ObservableCollection<StringItem>();

        public static List<LocalizedString> stringCache;

        public StringIdFieldControl(FieldInfo field, object fieldValue, GameCacheContext cacheContext)
        {
            InitializeComponent();

            if(stringCache == null)
            {
                stringCache = new List<LocalizedString>();
                using (var stream = cacheContext.OpenTagCacheRead())
                {
                    foreach (var unicTag in cacheContext.TagCache.Index.FindAllInGroup("unic"))
                    {
                        var unic = cacheContext.Deserializer.Deserialize<MultilingualUnicodeStringList>(new TagSerializationContext(stream, cacheContext, unicTag));
                        stringCache.AddRange(unic.Strings);
                    }
                }
            }

            this.cacheContext = cacheContext;
            this.field = field;

            cmbValue.Text = cacheContext.GetString((StringId)fieldValue);
            UpdateRecommendations();
            cmbValue.ItemsSource = recommendations;

            lblName.Content = field.Name;
            lblType.Content = field.FieldType.Name;
        }

        public void UpdateRecommendations()
        {
            recommendations.Clear();
            if (cmbValue.Text != "" && !cmbValue.Text.Contains(" "))
                foreach(var str in stringCache)
                    if (str.StringIDStr.Contains(cmbValue.Text))
                        recommendations.Add(new StringItem()
                        {
                            stringID = str.StringID,
                            value = str.StringIDStr
                        });
        }

        public class StringItem
        {
            public StringId stringID;
            public string value;

            public override string ToString()
            {
                return value;
            }
        }

        private void cmbValue_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            UpdateRecommendations();
        }
    }
}
