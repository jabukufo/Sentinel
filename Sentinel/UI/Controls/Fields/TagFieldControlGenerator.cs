﻿using System.Collections;
using System.Linq;
using System.Windows.Controls;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;

namespace Sentinel.UI.Controls.Fields
{
    public class TagFieldControlGenerator
    {
        public static void Generate(StackPanel view, GameCacheContext cacheContext, TagStructureInfo tagStructInfo, object deserializedTag)
        {
            var enumerator = new TagFieldEnumerator(tagStructInfo);

            while (enumerator.Next())
            {
                if (enumerator.Attribute != null && enumerator.Attribute.Padding == true)
                    continue;

                var fieldType = enumerator.Field.FieldType;
                var fieldValue = enumerator.Field.GetValue(deserializedTag);

                if (fieldType.GetInterface(typeof(IList).Name) != null && fieldType.GetGenericArguments().FirstOrDefault() != null)
                    view.Children.Add(new TagBlockFieldControl(enumerator.Field, (IList)fieldValue, cacheContext));
                else if (fieldType.IsEnum)
                    view.Children.Add(new EnumFieldControl(enumerator.Field, fieldValue));
                else if (fieldType == typeof(StringId))
                    view.Children.Add(new StringIdFieldControl(enumerator.Field, fieldValue, cacheContext));
                else if (fieldType == typeof(CachedTagInstance))
                    view.Children.Add(new TagReferenceFieldControl(enumerator.Field, (CachedTagInstance)fieldValue, cacheContext));
                else if (fieldType == typeof(RealPoint3d))
                    view.Children.Add(new Point3DFieldControl(enumerator.Field, (RealPoint3d)fieldValue));
                else
                    view.Children.Add(new GenericFieldControl(enumerator.Field, fieldValue, cacheContext));
            }
        }
    }
}
