﻿using System.Reflection;
using System.Windows.Controls;
using BlamCore.Cache;

namespace Sentinel.UI.Controls.Fields
{
    /// <summary>
    /// Interaction logic for GenericFieldControl.xaml
    /// </summary>
    public partial class GenericFieldControl : UserControl
    {
        FieldInfo field;

        public GenericFieldControl(FieldInfo field, object fieldValue, GameCacheContext cacheContext)
        {
            InitializeComponent();

            this.field = field;

            string valueString;
            if (fieldValue == null)
                valueString = "null";
            else
                valueString = fieldValue.ToString();

            lblName.Content = field.Name;
            txtValue.Text = valueString;
            lblType.Content = field.FieldType.Name;
        }
    }
}
