﻿using System.Reflection;
using System.Windows.Controls;
using BlamCore.Common;

namespace Sentinel.UI.Controls.Fields
{
    /// <summary>
    /// Interaction logic for Point3DFieldControl.xaml
    /// </summary>
    public partial class Point3DFieldControl : UserControl
    {
        public Point3DFieldControl(FieldInfo field, RealPoint3d fieldValue)
        {
            InitializeComponent();

            txtX.Text = fieldValue.X.ToString();
            txtY.Text = fieldValue.Y.ToString();
            txtZ.Text = fieldValue.Z.ToString();

            lblName.Content = field.Name;
            lblType.Content = field.FieldType.Name;
        }
    }
}
