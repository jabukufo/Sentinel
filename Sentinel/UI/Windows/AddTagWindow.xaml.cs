﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using BlamCore.TagDefinitions;
using MetroWPFTemplate.Metro.Native;

namespace Sentinel.UI.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddTagWindow : Window
    {
        public AddTagWindow()
        {
            this.CommandBindings.Add(new CommandBinding(
                ApplicationCommands.Close,
                OnExit));

            InitializeComponent();

            DwmDropShadow.DropShadowToWindow(this);

            TagSelection.Items.Clear();
            foreach (var tagType in TagDefinition.Types)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = tagType.Key.ToString();
                
                TagSelection.Items.Add(item);
            }

        }

        #region Metro

        private void headerThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Left = Left + e.HorizontalChange;
            Top = Top + e.VerticalChange;
        }
        private void btnActionSupport_Click(object sender, RoutedEventArgs e)
        {
            // Load support page?
        }
        private void btnActionClose_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion

        void OnExit(object target, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        public string TagType = null;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var item = (ComboBoxItem)TagSelection.SelectedItem;
            if(item != null)
            {
                string value = item.Content.ToString();
                TagType = value;
                this.DialogResult = true;

                this.Close();
            }
        }
    }
}
