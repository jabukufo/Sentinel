﻿using System.Windows;
using MetroWPFTemplate.Backend;
using MetroWPFTemplate.Metro.Controls.PageTemplates;

namespace Sentinel.UI.Tabs
{
    /// <summary>
    /// Interaction logic for StartPage.xaml
    /// </summary>
	public partial class StartPage : IPage
    {
        public StartPage()
        {
            InitializeComponent();

            // Save the Start Page
            Settings.StartPage = this;

            // Setup the UI
            mainContent.Visibility = Visibility.Visible;
        }
        public bool Close() { return true; }
    }
}
