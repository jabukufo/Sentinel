﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.TagDefinitions;
using MetroWPFTemplate.Metro.Controls.PageTemplates;
using MetroWPFTemplate.Metro.Controls.TabClosing;
using Sentinel.UI.Editors;
using Sentinel.UI.Windows;

namespace Sentinel.UI.Tabs
{
    /// <summary>
    /// Interaction logic for TagsPage.xaml
    /// </summary>
	public partial class TagsPage : IPage
    {
        public static App app = null;

        public static readonly DependencyProperty BlamTagProperty = DependencyProperty.Register("Tag", typeof(CachedTagInstance), typeof(TreeViewItem), new PropertyMetadata(null));

        private GameCacheContext CacheContext { get; set; }

        public TagsPage()
        {
            app = (App)Application.Current;

            InitializeComponent();

            // Setup the UI
            mainContent.Visibility = Visibility.Visible;
        }
        public bool Close() { return true; }

        public void AddTag(string type)
        {
            // Create tag instance
            Tag _tag = new Tag(type);
            Type tag_group = TagDefinition.Find(_tag);
            CachedTagInstance tag = CacheContext.TagCache.AllocateTag(TagGroup.Instances[_tag]);
            object deserializedTag = Activator.CreateInstance(TagDefinition.Find(type));

            CloseableTabItem tagTab = new CloseableTabItem();
            tagTab.Header = GetTagName(tag);
            tagTab.Content = new TagCacheEditor(CacheContext, deserializedTag);

            EditorTabs.Items.Insert(EditorTabs.Items.Count, tagTab);
            EditorTabs.SelectedIndex = EditorTabs.Items.Count - 1;
        }


        public void PopulateTagsList()
        {
            cacheTreeView.Items.Clear();

            foreach (var tagType in TagDefinition.Types)
            {

                var tagTypeNode = new TreeViewItem();
                tagTypeNode.Header = tagType.Key + " - " + tagType.Value.Name;
                var tags = CacheContext.TagCache.Index.FindAllInGroup(tagType.Key);

                foreach (var tag in tags)
                {
                    var tagNode = new TreeViewItem();
                    tagNode.Header = GetTagName(tag);
                    tagNode.SetValue(BlamTagProperty, tag);
                    tagNode.MouseDoubleClick += TagNode_Selected;
                    tagTypeNode.Items.Add(tagNode);
                }

                cacheTreeView.Items.Add(tagTypeNode);
            }


        }

        public void SetupCache(string fileName)
        {
            CacheContext = new GameCacheContext(new FileInfo(fileName).Directory);
            CacheContext.LoadTagNames();

            PopulateTagsList();

            //Filter out empty tag types
            UpdateFilter("");
        }

        private CloseableTabItem OpenTagTab(CachedTagInstance tag)
        {
            CloseableTabItem tags_tab = new CloseableTabItem();
            tags_tab.Header = GetTagName(tag);
            tags_tab.CloseTab += TagTab_CloseTab;
            tags_tab.Content = new TagCacheEditor(CacheContext, tag);
            EditorTabs.Items.Insert(EditorTabs.Items.Count, tags_tab);
            EditorTabs.SelectedIndex = EditorTabs.Items.Count - 1;

            // Implement extra custom tabs here
            // TODO: Move these tabs into an almalgimated tags tab group.
            Type tag_group = TagDefinition.Find(tag.Group.Tag);
            if (tag_group == typeof(Scenario))
            {
                CloseableTabItem scenario_tab = new CloseableTabItem();
                scenario_tab.Header = GetTagName(tag) + " View";
                scenario_tab.CloseTab += TagTab_CloseTab;

                var index = EditorTabs.SelectedIndex;
                EditorTabs.Items.Insert(EditorTabs.Items.Count, scenario_tab);
                EditorTabs.SelectedIndex = index;

                object deserializedTag;
                using (var stream = CacheContext.OpenTagCacheRead())
                {
                    deserializedTag = CacheContext.Deserializer.Deserialize(new TagSerializationContext(stream, CacheContext, tag), TagDefinition.Find(tag.Group.Tag));
                }
                scenario_tab.Content = new ScenarioEditor(CacheContext, tag, (Scenario)deserializedTag);

                
            }

            return tags_tab;
        }

        private void TagTab_CloseTab(object sender, RoutedEventArgs e)
        {
            TabItem tabItem = sender as TabItem;
            if (tabItem != null && EditorTabs.Items.Contains(tabItem))
                EditorTabs.Items.Remove(tabItem);
        }

        private void TagNode_Selected(object sender, RoutedEventArgs e)
        {
            TreeViewItem tagNode = (TreeViewItem)sender;
            CachedTagInstance tag = (CachedTagInstance)tagNode.GetValue(BlamTagProperty);

            // Open the tag tab if it already exists
            foreach (TabItem tab in EditorTabs.Items)
                if (tab.Header == tagNode.Header)
                {
                    EditorTabs.SelectedIndex = EditorTabs.Items.IndexOf(tab);
                    return;
                }

            OpenTagTab(tag);
        }

        private string GetTagName(CachedTagInstance tag)
        {
            return CacheContext.TagNames.ContainsKey(tag.Index) ? CacheContext.TagNames[tag.Index] : $"0x{tag.Index:X4}";
        }

        private void UpdateFilter(string filter)
        {
            foreach (TreeViewItem tagTypeNode in cacheTreeView.Items)
            {
                bool hasVisibleChildren = false;
                foreach (TreeViewItem tagNode in tagTypeNode.Items)
                    if (txtFilter.Text == "" || tagNode.Header.ToString().ToLower().Contains(filter.ToLower()))
                    {
                        hasVisibleChildren = true;
                        tagNode.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        tagNode.Visibility = Visibility.Collapsed;
                    }

                tagTypeNode.Visibility = (hasVisibleChildren) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateFilter(txtFilter.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var Window = new AddTagWindow();
            if (Window.ShowDialog().Value)
            {
                string res = Window.TagType;

                this.AddTag(res);
                }
        }
    }
}
