﻿using System.Windows.Controls;
using BlamCore.Cache;
using BlamCore.Serialization;
using BlamCore.TagDefinitions;
using Sentinel.UI.Controls.Fields;

namespace Sentinel.UI.Editors
{
    /// <summary>
    /// Interaction logic for TagCacheEditor.xaml
    /// </summary>
    public partial class TagCacheEditor : UserControl
    {
        public TagCacheEditor(GameCacheContext cacheContext, object deserializedTag)
        {
            InitializeComponent();

            setup(cacheContext, deserializedTag);
        }

        public TagCacheEditor(GameCacheContext cacheContext, CachedTagInstance tag)
        {
            InitializeComponent();

            object deserializedTag;
            using (var stream = cacheContext.OpenTagCacheRead())
            {
                deserializedTag = cacheContext.Deserializer.Deserialize(new TagSerializationContext(stream, cacheContext, tag), TagDefinition.Find(tag.Group.Tag));
            }

            setup(cacheContext, deserializedTag);
        }

        private void setup(GameCacheContext cacheContext, object deserializedTag)
        {
            TagStructureInfo tagStructInfo = new TagStructureInfo(deserializedTag.GetType(), cacheContext.Version);
            TagFieldControlGenerator.Generate(tagStructure, cacheContext, tagStructInfo, deserializedTag);
        }
    }
}
