﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using BlamCore.Bitmaps;
using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Geometry;
using BlamCore.TagDefinitions;
using BlamCore.TagResources;
using System.Reflection;
using System.Windows.Threading;

namespace Sentinel.UI.Editors
{
    public static class Helper
    {
        /// <summary>
        /// Copy an object to destination object, only matching fields will be copied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceObject">An object with matching fields of the destination object</param>
        /// <param name="destObject">Destination object, must already be created</param>
        public static void CopyObject<T>(object sourceObject, ref T destObject)
        {
            //  If either the source, or destination is null, return
            if (sourceObject == null || destObject == null)
                return;

            //  Get the type of each object
            Type sourceType = sourceObject.GetType();
            Type destType = destObject.GetType();

            //  Loop through the source properties
            var properties = sourceType.GetProperties();
            foreach (PropertyInfo p in properties)
            {
                //  Get the matching property in the destination object
                PropertyInfo targetObj = destType.GetProperty(p.Name);
                //  If there is none, skip
                if (targetObj == null)
                    continue;

                //  Set the value in the destination
                targetObj.SetValue(destObject, p.GetValue(sourceObject, null), null);
            }

            //  Loop through the source properties
            var fields = sourceType.GetFields();
            foreach (var f in fields)
            {
                //  Get the matching property in the destination object
                FieldInfo targetObj = destType.GetField(f.Name);
                //  If there is none, skip
                if (targetObj == null)
                    continue;

                //  Set the value in the destination
                targetObj.SetValue(destObject, f.GetValue(sourceObject));
            }
        }
    }

    /// <summary>
    /// Interaction logic for ScenarioEditor.xaml
    /// </summary>
    public partial class ScenarioEditor : UserControl
    {
        private Renderer Renderer;

        private FileInfo MapFile { get; set; }

        private GameCacheContext CacheContext { get; set; }

        private Scenario ScenarioDefinition { get; set; }

        private CachedTagInstance ScenarioInstance { get; set; }

        private ScenarioStructureBsp BspDefinition { get; set; }

        private DispatcherTimer Timer = new DispatcherTimer();

        public ScenarioEditor()
        {
            InitializeComponent();
        }

        public ScenarioEditor(GameCacheContext cacheContext, CachedTagInstance cache_instance, Scenario scenario)
        {
            InitializeComponent();

            SetupRenderer();
            LoadScenario(cacheContext, cache_instance, scenario);


            Timer = new DispatcherTimer();
            Timer.Tick += new EventHandler(Timer_Tick);
            Timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            Timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if(Renderer != null)
            {
                Renderer.Timer_Tick(sender, e);
            }
        }

        public void LoadMapFile(string path)
        {
            // Load The Map
            MapFile = new FileInfo(path);

            if (!MapFile.Exists)
                throw new FileNotFoundException(MapFile.FullName);
        }

        public void SetupRenderer()
        {
            Renderer = Viewport;

            Renderer.MouseDown += Renderer_MouseDown;
            Renderer.MouseMove += Renderer_MouseMove;
            scenarioTreeView.MouseDoubleClick += scenarioTreeView_DoubleClick;
        }

        private void Renderer_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
                return;

            // Get the mouse's position relative to the viewport.
            var mousePosition = e.GetPosition(Renderer.Viewport);

            // Perform the hit test.
            var result = VisualTreeHelper.HitTest(Renderer.Viewport, mousePosition) as RayMeshGeometry3DHitTestResult;

            if (result == null)
                return;

            var modelHit = (GeometryModel3D)result.ModelHit;

            foreach (var entry in GameObjects)
                if (entry.Meshes.Contains(modelHit))
                {
                    var modelGroup = entry.ModelGroup;
                    var radius = entry.Definition.BoundingRadius < 0.5f ? 0.5f : entry.Definition.BoundingRadius;

                    var xAxisMesh = MeshExtensions.XAxisArrow(radius);
                    modelGroup.Children.Add(xAxisMesh.SetMaterial(Brushes.Red, false));

                    var yAxisMesh = MeshExtensions.YAxisArrow(radius);
                    modelGroup.Children.Add(yAxisMesh.SetMaterial(Brushes.Green, false));

                    var zAxisMesh = MeshExtensions.ZAxisArrow(radius);
                    modelGroup.Children.Add(zAxisMesh.SetMaterial(Brushes.Blue, false));
                }
        }

        private void Renderer_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
        }

        private void SetupCamera()
        {
            var camera = (PerspectiveCamera)this.Renderer.Viewport.Camera;

            var XBounds = new Bounds<float>(float.MaxValue, float.MinValue);
            var YBounds = new Bounds<float>(float.MaxValue, float.MinValue);
            var ZBounds = new Bounds<float>(float.MaxValue, float.MinValue);

            foreach (var cluster in BspDefinition.Clusters)
            {
                if (cluster.MeshIndex >= BspDefinition.Geometry2.Meshes.Count)
                    continue;

                if (BspDefinition.Geometry2.Meshes[cluster.MeshIndex].Parts.Count == 0)
                    continue;

                if (cluster.BoundsX.Lower < XBounds.Lower)
                    XBounds = new Bounds<float>(cluster.BoundsX.Lower, XBounds.Upper);

                if (cluster.BoundsX.Upper > XBounds.Upper)
                    XBounds = new Bounds<float>(XBounds.Lower, cluster.BoundsX.Upper);

                if (cluster.BoundsY.Lower < YBounds.Lower)
                    YBounds = new Bounds<float>(cluster.BoundsY.Lower, YBounds.Upper);

                if (cluster.BoundsY.Upper > YBounds.Upper)
                    YBounds = new Bounds<float>(YBounds.Lower, cluster.BoundsY.Upper);

                if (cluster.BoundsZ.Lower < ZBounds.Lower)
                    ZBounds = new Bounds<float>(cluster.BoundsZ.Lower, ZBounds.Upper);

                if (cluster.BoundsZ.Upper > ZBounds.Upper)
                    ZBounds = new Bounds<float>(ZBounds.Lower, cluster.BoundsZ.Upper);
            }

            double pythagoras3d = Math.Sqrt(
                Math.Pow(XBounds.Upper - XBounds.Lower, 2) +
                Math.Pow(YBounds.Upper - YBounds.Lower, 2) +
                Math.Pow(ZBounds.Upper - ZBounds.Lower, 2));

            if (double.IsInfinity(pythagoras3d) || pythagoras3d == 0) //no clusters
            {
                XBounds = new Bounds<float>(BspDefinition.WorldBoundsX.Lower, BspDefinition.WorldBoundsX.Upper);
                YBounds = new Bounds<float>(BspDefinition.WorldBoundsY.Lower, BspDefinition.WorldBoundsY.Upper);
                ZBounds = new Bounds<float>(BspDefinition.WorldBoundsZ.Lower, BspDefinition.WorldBoundsZ.Upper);

                pythagoras3d = Math.Sqrt(
                    Math.Pow(XBounds.Upper - XBounds.Lower, 2) +
                    Math.Pow(YBounds.Upper - YBounds.Lower, 2) +
                    Math.Pow(ZBounds.Upper - ZBounds.Lower, 2));
            }

            var cameraPosition = new Point3D(
                XBounds.Upper + pythagoras3d * 0.5,
                (YBounds.Upper + YBounds.Lower) / 2,
                (ZBounds.Upper + ZBounds.Lower) / 2);

            foreach (var cameraPoint in ScenarioDefinition.CutsceneCameraPoints)
                if (cameraPoint.Name == "prematch_camera")
                {
                    cameraPosition = new Point3D(
                        cameraPoint.Position.X,
                        cameraPoint.Position.Y,
                        cameraPoint.Position.Z);

                    break;
                }

            Renderer.TranslateCamera(cameraPosition, new Vector3D(0, 0, -1));

            Renderer.CameraSpeed = Math.Ceiling(pythagoras3d * 3) / 5000;
            Renderer.MaxCameraSpeed = Math.Ceiling(pythagoras3d * 3) * 5 / 1000;

            Renderer.MaxPosition = new Point3D(
                BspDefinition.WorldBoundsX.Upper + pythagoras3d * 2,
                BspDefinition.WorldBoundsY.Upper + pythagoras3d * 2,
                BspDefinition.WorldBoundsZ.Upper + pythagoras3d * 2);

            Renderer.MinPosition = new Point3D(
                BspDefinition.WorldBoundsX.Lower - pythagoras3d * 2,
                BspDefinition.WorldBoundsY.Lower - pythagoras3d * 2,
                BspDefinition.WorldBoundsZ.Lower - pythagoras3d * 2);

            Renderer.FarPlaneMin = pythagoras3d * 0.01;
            Renderer.FarPlane = (pythagoras3d * pythagoras3d) / 2;
            Renderer.FarPlaneMax = pythagoras3d * pythagoras3d;

            ((PerspectiveCamera)Renderer.Viewport.Camera).FieldOfView = 90;
        }

        public Transform3DGroup CreateTransform(RealPoint3d position, RealEulerAngles3d eulerRot)
        {
            var mat = Matrix3D.Identity;

            mat.OffsetX = position.X;
            mat.OffsetY = position.Y;
            mat.OffsetZ = position.Z;

            var c1 = Math.Cos(eulerRot.Yaw.Radians * 0.5);
            var c2 = Math.Cos(eulerRot.Pitch.Radians * 0.5);
            var c3 = Math.Cos(eulerRot.Roll.Radians * 0.5);
            var s1 = Math.Sin(eulerRot.Yaw.Radians * 0.5);
            var s2 = Math.Sin(eulerRot.Pitch.Radians * 0.5);
            var s3 = Math.Sin(eulerRot.Roll.Radians * 0.5);

            mat.RotateAt(
                new Quaternion(
                    c1 * c2 * s3 - s1 * s2 * c3,
                    c1 * s2 * c3 + s1 * c2 * s3,
                    s1 * c2 * c3 - c1 * s2 * s3,
                    c1 * c2 * c3 + s1 * s2 * s3),
                new Point3D(position.X, position.Y, position.Z));

            var transform = new Transform3DGroup();
            transform.Children.Add(new MatrixTransform3D(mat));

            return transform;
        }

        public object GetField(object sourceObject, string sourceName)
        {
            Type sourceType = sourceObject.GetType();

            //  Loop through the source properties
            var field = sourceType.GetField(sourceName);

            return field.GetValue(sourceObject);
        }

        public void SetupGeneric<Tx, Ty>(List<Tx> _instances, List<Ty> _palletes, string pallet_field_name, Stream cacheStream, string name)
        {
            List<Scenario.IScenarioInstance> instances = new List<Scenario.IScenarioInstance>();

            foreach (object instance in _instances)
            {
                Scenario.IScenarioInstance new_instance = new Scenario.IScenarioInstance();
                Helper.CopyObject<Scenario.IScenarioInstance>(instance, ref new_instance);
                instances.Add(new_instance);
            }

            List<CachedTagInstance> palletes = new List<CachedTagInstance>();
            foreach (object pallete in _palletes)
            {
                object value = GetField(pallete, pallet_field_name);
                palletes.Add((CachedTagInstance)value);
            }

            Renderer.StatsLabel.Content = $"Loading {name}s...";
            Renderer.Refresh();

            var rootNode = new TreeViewItem() { Header = name };

            var modelGroup = new Model3DGroup();

            foreach (var entry in instances)
            {
                var index = entry.PaletteIndex;
                // Error Checking
                {
                    if (((entry.PlacementFlags & Scenario.ObjectPlacementFlags.NotAutomatically) != 0) && ((entry.PlacementFlags & Scenario.ObjectPlacementFlags.NeverPlaced) != 0))
                        continue;

                    // Ensure the index is within the BipedPallette Count
                    if (index < 0 || index >= palletes.Count)
                        continue;
                }

                var definition = palletes[index];
                var model = LoadGameObject(cacheStream, definition);

                // Ensure the model is valid
                if (model == null) continue;

                // Position and rotation
                var position = new RealPoint3d(entry.Position.X, entry.Position.Y, entry.Position.Z);
                var eulerRot = new RealEulerAngles3d(entry.Rotation.Yaw, entry.Rotation.Pitch, entry.Rotation.Roll);

                // Setup the ModelGroup
                var sectionGroup = new Model3DGroup() { Transform = CreateTransform(position, eulerRot) };
                sectionGroup.Children.Add(model);
                modelGroup.Children.Add(sectionGroup);

                // Create a TreeView Entry
                var treeNode = new TreeViewItem();

                if(entry.NameIndex >= 0 && ScenarioDefinition.ObjectNames.Count > 0)
                {
                    treeNode.Header = ScenarioDefinition.ObjectNames[entry.NameIndex].Name;
                } else
                {
                    treeNode.Header = $"{name} {instances.IndexOf(entry) + 1}";
                }

                treeNode.Tag = position;
                rootNode.Items.Add(treeNode);
            }

            scenarioTreeView.Items.Add(rootNode);
            Renderer.Viewport.Children.Add(new ModelVisual3D { Content = modelGroup });
        }

        public void LoadScenario(GameCacheContext cacheContext, CachedTagInstance cache_instance, Scenario scenario)
        {
            CacheContext = cacheContext;
            ScenarioDefinition = scenario;
            ScenarioInstance = cache_instance;

            using (var cacheStream = CacheContext.OpenTagCacheRead())
            {
                var context = new TagSerializationContext(cacheStream, CacheContext, CacheContext.GetTag(ScenarioInstance.Index)); //TODO: Totally not sure if this is a good idea

                Renderer.StatsLabel.Content = "Loading bsp definition...";
                Renderer.Refresh();

                context = new TagSerializationContext(cacheStream, CacheContext, ScenarioDefinition.StructureBsps[0].StructureBsp);
                BspDefinition = CacheContext.Deserializer.Deserialize<ScenarioStructureBsp>(context);

                SetupCamera();

                Renderer.StatsLabel.Content = "Loading atmosphere...";
                Renderer.Refresh();

                context = new TagSerializationContext(cacheStream, CacheContext, ScenarioDefinition.SkyReferences[0].SkyObject);
                object skyObject = null;
                SkyAtmParameters skya = null;

                // Load the Sky Attributes
                {
                    if (ScenarioDefinition.SkyReferences != null && ScenarioDefinition.SkyReferences.Count > 0 && ScenarioDefinition.SkyReferences[0].SkyObject != null)
                    {
                        skyObject = CacheContext.Deserializer.Deserialize(context, TagDefinition.Find(ScenarioDefinition.SkyReferences[0].SkyObject.Group.Tag));
                    }

                    if (ScenarioDefinition.SkyParameters != null)
                    {
                        context = new TagSerializationContext(cacheStream, CacheContext, ScenarioDefinition.SkyParameters);
                        skya = CacheContext.Deserializer.Deserialize<SkyAtmParameters>(context);
                    }


                }

                var lightGroup = new Model3DGroup();

                lightGroup.Children.Add(new AmbientLight(Colors.White));
                Renderer.Viewport.Children.Add(new ModelVisual3D { Content = lightGroup });

                var skyGroups = LoadGameObject(cacheStream, ScenarioDefinition.SkyReferences[0].SkyObject);
                Renderer.Viewport.Children.Add(new ModelVisual3D { Content = skyGroups });

                using (var bspStream = new MemoryStream())
                {
                    var bspTag = ScenarioDefinition.StructureBsps[0].StructureBsp;
                    var groupName = CacheContext.GetString(bspTag.Group.Name);

                    if (!CacheContext.TagNames.ContainsKey(bspTag.Index))
                        CacheContext.TagNames[bspTag.Index] = $"0x{bspTag.Index:X8}";

                    Renderer.InfoLabel.Content = $"Loading {CacheContext.TagNames[bspTag.Index]}.{groupName}...";
                    Renderer.Refresh();

                    CacheContext.ExtractResource(BspDefinition.Geometry2.Resource, bspStream);

                    var bspContext = new ResourceSerializationContext(BspDefinition.Geometry2.Resource);
                    var bspGeometry = CacheContext.Deserializer.Deserialize<RenderGeometryApiResourceDefinition>(bspContext);

                    #region BSP Materials
                    var materials = new List<MaterialGroup>();

                    Renderer.StatsLabel.Content = "Loading bsp materials...";
                    Renderer.Refresh();

                    foreach (var material in BspDefinition.Materials)
                    {
                        var m = (material.RenderMethod != null) ? LoadMaterial(cacheStream, material.RenderMethod) : null;
                        materials.Add(m);
                    }
                    #endregion

                    #region BSP Clusters
                    var clusters = new Model3DGroup();

                    Renderer.StatsLabel.Content = "Loading bsp clusters...";
                    Renderer.Refresh();

                    foreach (var cluster in BspDefinition.Clusters)
                    {
                        var meshes = LoadRenderGeometry(
                            bspTag,
                            BspDefinition.Geometry2.Meshes[cluster.MeshIndex],
                            materials, bspStream, bspGeometry);

                        if (meshes == null)
                            continue;

                        clusters.Children.Add(meshes);
                    }

                    Renderer.Viewport.Children.Add(new ModelVisual3D { Content = clusters });
                    #endregion

                    #region BSP Instanced Geometry
                    var instancedGeometry = new Model3DGroup();

                    Renderer.StatsLabel.Content = "Loading bsp instanced geometry...";
                    Renderer.Refresh();

                    foreach (var instance in BspDefinition.InstancedGeometryInstances)
                    {
                        var section = LoadRenderGeometry(
                            bspTag,
                            BspDefinition.Geometry2.Meshes[instance.MeshIndex],
                            materials, bspStream, bspGeometry,
                            BspDefinition.Geometry2.Compression[instance.MeshIndex]);

                        if (section == null)
                            continue;

                        var mat = Matrix3D.Identity;
                        mat.M11 = instance.Matrix.m11;
                        mat.M12 = instance.Matrix.m12;
                        mat.M13 = instance.Matrix.m13;
                        mat.M21 = instance.Matrix.m21;
                        mat.M22 = instance.Matrix.m22;
                        mat.M23 = instance.Matrix.m23;
                        mat.M31 = instance.Matrix.m31;
                        mat.M32 = instance.Matrix.m32;
                        mat.M33 = instance.Matrix.m33;
                        mat.OffsetX = instance.Matrix.m41;
                        mat.OffsetY = instance.Matrix.m42;
                        mat.OffsetZ = instance.Matrix.m43;

                        var transform = new Transform3DGroup();
                        transform.Children.Add(new ScaleTransform3D(instance.Scale, instance.Scale, instance.Scale));
                        transform.Children.Add(new MatrixTransform3D(mat));

                        var sectionGroup = new Model3DGroup();
                        sectionGroup.Children.Add(section);
                        sectionGroup.Transform = transform;

                        instancedGeometry.Children.Add(sectionGroup);
                    }

                    Renderer.Viewport.Children.Add(new ModelVisual3D { Content = instancedGeometry });
                    #endregion
                }

                SetupGeneric(
                    ScenarioDefinition.Bipeds,
                    ScenarioDefinition.BipedPalette,
                    "Biped",
                    cacheStream,
                    "biped"
                    );
                SetupGeneric(
                    ScenarioDefinition.Vehicles,
                    ScenarioDefinition.VehiclePalette,
                    "Vehicle",
                    cacheStream,
                    "vehicle"
                    );
                SetupGeneric(
                    ScenarioDefinition.Scenery,
                    ScenarioDefinition.SceneryPalette,
                    "Scenery",
                    cacheStream,
                    "scenery"
                    );
                SetupGeneric(
                    ScenarioDefinition.Equipment,
                    ScenarioDefinition.EquipmentPalette,
                    "Equipment",
                    cacheStream,
                    "equipment"
                    );
                SetupGeneric(
                    ScenarioDefinition.Weapons,
                    ScenarioDefinition.WeaponPalette,
                    "Weapon",
                    cacheStream,
                    "weapon"
                    );
                SetupGeneric(
                    ScenarioDefinition.Machines,
                    ScenarioDefinition.MachinePalette,
                    "Machine",
                    cacheStream,
                    "machine");
                SetupGeneric(
                    ScenarioDefinition.Terminals,
                    ScenarioDefinition.TerminalPalette,
                    "Terminal",
                    cacheStream,
                    "terminal"
                    );
                SetupGeneric(
                    ScenarioDefinition.AlternateRealityDevices,
                    ScenarioDefinition.AlternateRealityDevicePalette,
                    "ArgDevice",
                    cacheStream,
                    "alternate reality device"
                    );
                SetupGeneric(
                    ScenarioDefinition.Controls,
                    ScenarioDefinition.ControlPalette,
                    "Control",
                    cacheStream,
                    "control"
                    );
                SetupGeneric(
                    ScenarioDefinition.Giants,
                    ScenarioDefinition.GiantPalette,
                    "Giant",
                    cacheStream,
                    "giant"
                    );
                SetupGeneric(
                   ScenarioDefinition.EffectScenery,
                   ScenarioDefinition.EffectSceneryPalette,
                   "EffectScenery",
                   cacheStream,
                   "effect scenery"
                   );
                SetupGeneric(
                   ScenarioDefinition.LightVolumes,
                   ScenarioDefinition.LightVolumePalette,
                   "LightVolume",
                   cacheStream,
                   "light volume"
                   );
                SetupGeneric(
                   ScenarioDefinition.Crates,
                   ScenarioDefinition.CratePalette,
                   "Crate",
                   cacheStream,
                   "crate"
                   );
            }

            var scenarioIndex = cache_instance.Index;

            Renderer.StatsLabel.Content = null;
            Renderer.IsHitTestVisible = true;
            Renderer.Refresh();

            var buildDate = DateTime.FromFileTime(CacheContext.TagCache.Timestamp);

            var label =
                string.Format("Halo Online {0} {1} ({2})",
                    CacheVersionDetection.GetBuildName(CacheContext.Version),
                    buildDate.ToShortDateString(),
                    buildDate.ToShortTimeString());

            if (!CacheContext.TagNames.ContainsKey(scenarioIndex))
                CacheContext.TagNames[scenarioIndex] = $"0x{scenarioIndex:X8}";

            Renderer.InfoLabel.Content = $"{label} | {CacheContext.TagNames[scenarioIndex]}.scenario";

            Renderer.Start();
        }

        public void LoadScenario()
        {
            if (!MapFile.Exists)
                throw new FileNotFoundException(MapFile.FullName);

            var scenarioIndex = -1;

            using (var reader = new BinaryReader(MapFile.OpenRead()))
            {
                reader.BaseStream.Seek(4, SeekOrigin.Current);

                var mapFileVersion = reader.ReadInt32();

                if (mapFileVersion != 18)
                    throw new NotImplementedException("Only Halo Online map files are supported.");

                reader.BaseStream.Seek(0x011C, SeekOrigin.Begin);
                var buildName = new string(reader.ReadChars(32)).Trim();

                if (buildName.StartsWith("1.106708"))
                    reader.BaseStream.Seek(0x2DF0, SeekOrigin.Begin);
                else if (buildName.StartsWith("12.1.700123"))
                    reader.BaseStream.Seek(0x2E00, SeekOrigin.Begin);

                scenarioIndex = reader.ReadInt32();
            }

            Renderer.StatsLabel.Content = "Loading cache files...";
            Renderer.Refresh();

            CacheContext = new GameCacheContext(MapFile.Directory);

            Renderer.StatsLabel.Content = "Loading scenario definition...";
            Renderer.Refresh();


            using (var cacheStream = CacheContext.OpenTagCacheRead())
            {
                var context = new TagSerializationContext(cacheStream, CacheContext, CacheContext.GetTag(scenarioIndex));
                ScenarioInstance = context.GetTagByIndex(scenarioIndex);
                ScenarioDefinition = CacheContext.Deserializer.Deserialize<Scenario>(context);
            }


            LoadScenario(CacheContext, ScenarioInstance, ScenarioDefinition);


        }



        private void LoadTagDependencies(int index, ref HashSet<int> tags)
        {
            var queue = new List<int> { index };

            while (queue.Count != 0)
            {
                var nextQueue = new List<int>();

                foreach (var entry in queue)
                    if (!tags.Contains(entry))
                    {
                        if (CacheContext.TagCache.Index[entry] == null)
                            continue;

                        tags.Add(entry);

                        foreach (var dependency in CacheContext.TagCache.Index[entry].Dependencies)
                            if (!nextQueue.Contains(dependency))
                                nextQueue.Add(dependency);
                    }

                queue = nextQueue;
            }
        }

        private Dictionary<int, System.Drawing.Bitmap> Bitmaps = null;

        private System.Drawing.Bitmap LoadBitmap(Stream cacheStream, CachedTagInstance bitmapTag)
        {
            if (bitmapTag == null)
                return null;

            var groupName = CacheContext.GetString(bitmapTag.Group.Name);

            if (!CacheContext.TagNames.ContainsKey(bitmapTag.Index))
                CacheContext.TagNames[bitmapTag.Index] = $"0x{bitmapTag.Index:X8}";

            Renderer.InfoLabel.Content = $"Loading {CacheContext.TagNames[bitmapTag.Index]}.{groupName}...";
            Renderer.Refresh();

            if (Bitmaps == null)
                Bitmaps = new Dictionary<int, System.Drawing.Bitmap>();

            if (Bitmaps.ContainsKey(bitmapTag.Index))
                return Bitmaps[bitmapTag.Index];

            var context = new TagSerializationContext(cacheStream, CacheContext, bitmapTag);
            var bitmap = CacheContext.Deserializer.Deserialize<Bitmap>(context);
            var extractor = new BitmapDdsExtractor(CacheContext);

            System.Drawing.Bitmap result = null;

            using (var ddsStream = new MemoryStream())
            {
                extractor.ExtractDds(CacheContext.Deserializer, bitmap, 0, ddsStream);

                // Create a DevIL image "name" (which is actually a number)
                int img_name;
                DevIL.ilGenImages(1, out img_name);
                DevIL.ilBindImage(img_name);

                var ddsData = ddsStream.ToArray();

                // Load the DDS file into the bound DevIL image
                DevIL.ilLoadL(DevIL.IL_DDS, ddsData, ddsData.Length);

                // Set a few size variables that will simplify later code

                int ImgWidth = DevIL.ilGetInteger(DevIL.IL_IMAGE_WIDTH);
                int ImgHeight = DevIL.ilGetInteger(DevIL.IL_IMAGE_HEIGHT);
                var rect = new System.Drawing.Rectangle(0, 0, ImgWidth, ImgHeight);

                // Convert the DevIL image to a pixel byte array to copy into Bitmap
                DevIL.ilConvertImage(DevIL.IL_BGRA, DevIL.IL_UNSIGNED_BYTE);

                // Create a Bitmap to copy the image into, and prepare it to get data
                result = new System.Drawing.Bitmap(ImgWidth, ImgHeight);
                var bmd = result.LockBits(rect, System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                // Copy the pixel byte array from the DevIL image to the Bitmap
                DevIL.ilCopyPixels(0, 0, 0,
                  DevIL.ilGetInteger(DevIL.IL_IMAGE_WIDTH),
                  DevIL.ilGetInteger(DevIL.IL_IMAGE_HEIGHT),
                  1, DevIL.IL_BGRA, DevIL.IL_UNSIGNED_BYTE,
                  bmd.Scan0);

                // Clean up and return Bitmap
                DevIL.ilDeleteImages(1, ref img_name);
                result.UnlockBits(bmd);

                ddsStream.Close();
                ddsStream.Dispose();
            }

            Bitmaps[bitmapTag.Index] = result;
            return result;
        }

        private Dictionary<int, MaterialGroup> Materials = null;

        private MaterialGroup LoadMaterial(Stream cacheStream, CachedTagInstance tag)
        {
            var errMat = new MaterialGroup();
            errMat.Children.Add(new EmissiveMaterial { Color = Colors.Gold });

            var groupName = CacheContext.GetString(tag.Group.Name);

            if (!CacheContext.TagNames.ContainsKey(tag.Index))
                CacheContext.TagNames[tag.Index] = $"0x{tag.Index:X8}";

            Renderer.InfoLabel.Content = $"Loading {CacheContext.TagNames[tag.Index]}.{groupName}...";
            Renderer.Refresh();

            if (tag == null)
                return errMat;

            if (Materials == null)
                Materials = new Dictionary<int, MaterialGroup>();

            if (Materials.ContainsKey(tag.Index))
                return Materials[tag.Index];

            if (!tag.IsInGroup("rm  "))
            {
                Materials[tag.Index] = errMat;
                return errMat;
            }

            var context = new TagSerializationContext(cacheStream, CacheContext, tag);
            var shader = (RenderMethod)CacheContext.Deserializer.Deserialize(context, TagDefinition.Find(tag.Group.Tag));

            if (shader == null)
            {
                Materials[tag.Index] = errMat;
                return errMat;
            }

            context = new TagSerializationContext(cacheStream, CacheContext, shader.ShaderProperties[0].Template);
            var template = CacheContext.Deserializer.Deserialize<RenderMethodTemplate>(context);

            if (template == null)
            {
                Materials[tag.Index] = errMat;
                return errMat;
            }

            var bitmapIndex = -1;
            var bitmapArgName = "";
            var bitmapArgID = StringId.Null;

            for (var i = 0; i < template.ShaderMaps.Count; i++)
            {
                var usage = template.ShaderMaps[i];
                var name = CacheContext.GetString(usage.Name);

                if (name.StartsWith("diffuse_map") || name.StartsWith("base_map") || name == "foam_texture")
                    if (bitmapIndex == -1)
                    {
                        bitmapIndex = i;
                        bitmapArgName = name;
                        bitmapArgID = usage.Name;
                    }
            }

            CachedTagInstance bitmapTag = null;

            try
            {
                var shaderMap = shader.ShaderProperties[0].ShaderMaps[bitmapIndex];
                bitmapTag = shaderMap.Bitmap;
            }
            catch
            {
                Materials[tag.Index] = errMat;
                return errMat;
            }

            var tileIndex = shader.ShaderProperties[0].ShaderMaps[bitmapIndex].UvArgumentIndex;

            float uTiling;
            try { uTiling = shader.ShaderProperties[0].Arguments[tileIndex].Arg1; }
            catch { uTiling = 1; }

            float vTiling;
            try { vTiling = shader.ShaderProperties[0].Arguments[tileIndex].Arg2; }
            catch { vTiling = 1; }

            var material = new MaterialGroup();

            var diffuse = LoadBitmap(cacheStream, bitmapTag);

            if (diffuse == null)
            {
                Materials[tag.Index] = errMat;
                return errMat;
            }

            MemoryStream stream;

            try
            {
                stream = new MemoryStream();
                diffuse.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
            }
            catch
            {
                return null;
            }

            var image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();

            material.Children.Add(
                new DiffuseMaterial()
                {
                    Brush = new ImageBrush(image)
                    {
                        ViewportUnits = BrushMappingMode.Absolute,
                        TileMode = TileMode.Tile,
                        Viewport = new System.Windows.Rect(0, 0, 1f / Math.Abs(uTiling), 1f / Math.Abs(vTiling))
                    }
                });

            Materials[tag.Index] = material;
            return material;
        }

        private Model3DGroup LoadRenderGeometry(CachedTagInstance tag, Mesh mesh, List<MaterialGroup> materials, Stream resourceStream, RenderGeometryApiResourceDefinition resource, GeometryCompressionInfo compression = null, List<GeometryModel3D> models = null)
        {
            if (models == null)
                models = new List<GeometryModel3D>();

            if (mesh.Parts.Count == 0)
                return null;

            var vertexStream = VertexStreamFactory.Create(CacheContext.Version, resourceStream);

            var compressor = compression == null ? null : new VertexCompressor(compression);

            var group = new Model3DGroup();

            if (materials == null)
            {
                materials = new List<MaterialGroup>();
                var matGroup = new MaterialGroup();
                matGroup.Children.Add(new EmissiveMaterial() { Color = Colors.Gold });
                materials.Add(matGroup);
            }

            var vertexBuffer = resource.VertexBuffers[mesh.VertexBuffers[0]].Definition;
            var indexBuffer = resource.IndexBuffers[mesh.IndexBuffers[0]].Definition;

            foreach (var part in mesh.Parts)
            {
                var partMesh = new MeshGeometry3D();

                resourceStream.Position = indexBuffer.Data.Address.Offset + (part.FirstIndex * 2);

                var indexStream = new IndexBufferStream(resourceStream, IndexBufferFormat.UInt16);

                if (!(indexBuffer.Type == PrimitiveType.TriangleList || indexBuffer.Type == PrimitiveType.TriangleStrip))
                    throw new NotImplementedException(indexBuffer.Type.ToString());

                var indices = (indexBuffer.Type == PrimitiveType.TriangleStrip) ?
                    indexStream.ReadTriangleStrip(part.IndexCount) :
                    indexStream.ReadIndices(part.IndexCount);

                for (var i = 0; i < indices.Length; i++)
                {
                    resourceStream.Position = vertexBuffer.Data.Address.Offset + (RenderModelBuilder.VertexSizes[vertexBuffer.Format] * indices[i]);

                    switch (vertexBuffer.Format)
                    {
                        case VertexBufferFormat.Rigid:
                            var rigid = vertexStream.ReadRigidVertex();
                            if (compressor != null)
                            {
                                rigid.Position = compressor.DecompressPosition(rigid.Position);
                                rigid.Texcoord = compressor.DecompressUv(rigid.Texcoord);
                            }
                            partMesh.Positions.Add(new Point3D(rigid.Position.I, rigid.Position.J, rigid.Position.K));
                            partMesh.Normals.Add(new Vector3D(rigid.Normal.I, rigid.Normal.J, rigid.Normal.K));
                            partMesh.TextureCoordinates.Add(new Point(rigid.Texcoord.I, rigid.Texcoord.J));
                            break;

                        case VertexBufferFormat.Skinned:
                            var skinned = vertexStream.ReadSkinnedVertex();
                            if (compressor != null)
                            {
                                skinned.Position = compressor.DecompressPosition(skinned.Position);
                                skinned.Texcoord = compressor.DecompressUv(skinned.Texcoord);
                            }
                            partMesh.Positions.Add(new Point3D(skinned.Position.I, skinned.Position.J, skinned.Position.K));
                            partMesh.Normals.Add(new Vector3D(skinned.Normal.I, skinned.Normal.J, skinned.Normal.K));
                            partMesh.TextureCoordinates.Add(new Point(skinned.Texcoord.I, skinned.Texcoord.J));
                            break;

                        case VertexBufferFormat.World:
                            var world = vertexStream.ReadWorldVertex();
                            if (compressor != null)
                            {
                                world.Position = compressor.DecompressPosition(world.Position);
                                world.Texcoord = compressor.DecompressUv(world.Texcoord);
                            }
                            partMesh.Positions.Add(new Point3D(world.Position.I, world.Position.J, world.Position.K));
                            partMesh.Normals.Add(new Vector3D(world.Normal.I, world.Normal.J, world.Normal.K));
                            partMesh.TextureCoordinates.Add(new Point(world.Texcoord.I, world.Texcoord.J));
                            break;

                        default:
                            throw new NotImplementedException(vertexBuffer.Format.ToString());
                    }

                    partMesh.TriangleIndices.Add(i);
                }

                var partModel = new GeometryModel3D(partMesh, materials[part.MaterialIndex]);

                models.Add(partModel);
                group.Children.Add(partModel);
            }

            return group;
        }

        private List<GameObjectInfo> GameObjects { get; } = new List<GameObjectInfo>();

        private class GameObjectInfo
        {
            public CachedTagInstance Tag { get; set; }
            public GameObject Definition { get; set; }
            public Model3DGroup ModelGroup { get; set; } = new Model3DGroup();
            public List<GeometryModel3D> Meshes { get; set; } = new List<GeometryModel3D>();
        }

        private Model3DGroup LoadGameObject(Stream cacheStream, CachedTagInstance tag)
        {
            if (tag == null || !tag.IsInGroup(new Tag("obje")))
                return null;

            var groupName = CacheContext.GetString(tag.Group.Name);

            if (!CacheContext.TagNames.ContainsKey(tag.Index))
                CacheContext.TagNames[tag.Index] = $"0x{tag.Index:X8}";

            Renderer.InfoLabel.Content = $"Loading {CacheContext.TagNames[tag.Index]}.{groupName}...";
            Renderer.Refresh();

            var context = new TagSerializationContext(cacheStream, CacheContext, tag);
            var gameObject = (GameObject)CacheContext.Deserializer.Deserialize(context, TagDefinition.Find(tag.Group.Tag));
            var modelTag = gameObject.Model;

            if (modelTag == null)
                return null;

            context = new TagSerializationContext(cacheStream, CacheContext, gameObject.Model);
            var model = CacheContext.Deserializer.Deserialize<Model>(context);
            var renderModelTag = model.RenderModel;

            if (renderModelTag == null)
                return null;

            context = new TagSerializationContext(cacheStream, CacheContext, model.RenderModel);
            var renderModel = CacheContext.Deserializer.Deserialize<RenderModel>(context);

            var materials = new List<MaterialGroup>();
            foreach (var material in renderModel.Materials)
            {
                var m = LoadMaterial(cacheStream, material.RenderMethod);
                materials.Add(m);
            }

            var gameObjectInfo = new GameObjectInfo { Tag = tag, Definition = gameObject };

            using (var renderModelStream = new MemoryStream())
            {
                CacheContext.ExtractResource(renderModel.Geometry.Resource, renderModelStream);

                var renderModelContext = new ResourceSerializationContext(renderModel.Geometry.Resource);
                var renderModelGeometry = CacheContext.Deserializer.Deserialize<RenderGeometryApiResourceDefinition>(renderModelContext);

                foreach (var region in renderModel.Regions)
                {
                    // TODO: determine default permutation
                    if (region.Permutations == null || region.Permutations.Count == 0)
                        continue;

                    var permutation = region.Permutations[0];

                    for (var i = 0; i < permutation.MeshCount; i++)
                    {
                        var section = LoadRenderGeometry(
                            renderModelTag,
                            renderModel.Geometry.Meshes[permutation.MeshIndex + i],
                            materials, renderModelStream, renderModelGeometry,
                            renderModel.Geometry.Compression[0],
                            gameObjectInfo.Meshes);

                        gameObjectInfo.ModelGroup.Children.Add(section);
                    }
                }

                renderModelStream.Close();
                renderModelStream.Dispose();
            }

            GameObjects.Add(gameObjectInfo);

            return gameObjectInfo.ModelGroup;
        }

        private void scenarioTreeView_DoubleClick(object sender, EventArgs e)
        {

            if (scenarioTreeView.SelectedItem == null)
                return;

            TreeViewItem tvi = scenarioTreeView.SelectedItem as TreeViewItem;

            if (tvi == null || tvi.Tag == null)
                return;


            var point = (RealPoint3d)tvi.Tag;
            var vector = new RealVector3d(point.X, point.Y, point.Z);

            Renderer.TranslateCamera(new Point3D(vector.I, vector.J, vector.K), new Vector3D(-5, -10, 0));
        }
    }
}